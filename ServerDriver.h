#pragma once

#include <algorithm>
#include <chrono>
using namespace std::chrono;

#include <openvr_driver.h>
using namespace vr;

#include "DiscoTracker.h"

class ServerDriver : public IServerTrackedDeviceProvider
{
public:
	ServerDriver();

	~ServerDriver();

	EVRInitError Init(IVRDriverContext *pDriverContext) override;

	void Cleanup() override;

	const char * const *GetInterfaceVersions() override;

	void RunFrame() override;

	bool ShouldBlockStandbyMode() override;
    
	void EnterStandby() override;

	void LeaveStandby() override;

private:
	DiscoTracker tracker_foot_left;

	DiscoTracker tracker_foot_right;
};

